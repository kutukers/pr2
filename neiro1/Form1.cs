﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace neiro1
{
	public partial class Form1 : Form
	{
		static Random rnd = new Random();
		
		private int[] Xi = new int[15];
		private double[] Wi = new double[15];
		private int O = rnd.Next(1, 3);
		private bool y;
		double sum = 0;

		public double GetRandomNumber(double minimum, double maximum)
		{
			return rnd.NextDouble() * (maximum - minimum) + minimum;
		}

		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			for (int i = 0; i < 15; i++)
			{
				Wi[i] = GetRandomNumber(0, 1);
				textBox1.Text += $"{Wi[i]}, ";
			}
			

		}

		public int ChangeColor(Button button)
		{
			if (button.BackColor == SystemColors.Window)
			{
				button.BackColor = SystemColors.WindowText;
				return 1;
			}
			else
				button.BackColor = SystemColors.Window;

			return 0;

		}

		
			private void button1_Click(object sender, EventArgs e)
			{
				Xi[0] = ChangeColor(button1);
				
			}

			private void button2_Click(object sender, EventArgs e)
			{
			Xi[1] = ChangeColor(button2);
			}

			private void button3_Click(object sender, EventArgs e)
			{
			Xi[2] = ChangeColor(button3);
			}

			private void button4_Click(object sender, EventArgs e)
			{
			Xi[5] = ChangeColor(button4);
			}

			private void button5_Click(object sender, EventArgs e)
			{
			Xi[4] = ChangeColor(button5);
			}

			private void button6_Click(object sender, EventArgs e)
			{
				Xi[3] = ChangeColor(button6);
			}

			private void button7_Click(object sender, EventArgs e)
			{
			Xi[8] = ChangeColor(button7);
			}

			private void button8_Click(object sender, EventArgs e)
			{
				Xi[7] = ChangeColor(button8);
			}

			private void button9_Click(object sender, EventArgs e)
			{
			Xi[6] = ChangeColor(button9);
			}

			private void button10_Click(object sender, EventArgs e)
			{
			Xi[11] = ChangeColor(button10);
			}

			private void button11_Click(object sender, EventArgs e)
			{
				Xi[10] = ChangeColor(button11);
			}

			private void button12_Click(object sender, EventArgs e)
			{
			Xi[9] = ChangeColor(button12);
			}

			private void button13_Click(object sender, EventArgs e)
			{
			Xi[14] = ChangeColor(button13);
			}

			private void button14_Click(object sender, EventArgs e)
			{
			Xi[13] = ChangeColor(button14);
			}

			private void button15_Click(object sender, EventArgs e)
			{
			Xi[12] = ChangeColor(button15);
			}

		private void button16_Click(object sender, EventArgs e)
		{
			textBox2.Text = null;
			for (int i = 0; i < 15; i++)
			{
				textBox2.Text += $"{Xi[i]}, ";
			}
			textBox4.Text = $"{O}";

			if (getSum(Xi, Wi) >= O)
			{
				y = true;
			}
			else
			{
				y = false;
			}

			if (y)
			{
				textBox5.Text = $"парне ({y})";
			}
			else
				textBox5.Text = $"не парне ({y})";

		}

		public double getSum(int[] xi, double[] wi)
		{
			sum = 0;
			for (int i = 0; i < 15; i++)
			{
				sum += xi[i] * wi[i];
			}

			textBox3.Text = $"{sum}";
			return sum;
		}

		private void textBox4_TextChanged(object sender, EventArgs e)
		{
			
		}

		private void button17_Click(object sender, EventArgs e)
		{
			sum = 0;
			if (y)
			{
				for (int i = 0; i < 15; i++)
				{
					Wi[i] = Wi[i] - Xi[i];
				}
			}
			if (!y)
			{
				for (int i = 0; i < 15; i++)
				{
					Wi[i] = Wi[i] + Xi[i];
				}
			}

			for (int i = 0; i < 15; i++)
				textBox1.Text += $"{Wi[i]}";
			textBox3.Text = $"{sum}";
		}

		private void button18_Click(object sender, EventArgs e)
		{
			sum = 0;
			for (int i = 0; i < 15; i++)
			{
				Xi[12] = 0;
			}
			button1.BackColor = SystemColors.Window;
			button2.BackColor = SystemColors.Window;
			button3.BackColor = SystemColors.Window;
			button4.BackColor = SystemColors.Window;
			button5.BackColor = SystemColors.Window;
			button6.BackColor = SystemColors.Window;
			button7.BackColor = SystemColors.Window;
			button8.BackColor = SystemColors.Window;
			button9.BackColor = SystemColors.Window;
			button10.BackColor = SystemColors.Window;
			button11.BackColor = SystemColors.Window;
			button12.BackColor = SystemColors.Window;
			button13.BackColor = SystemColors.Window;
			button14.BackColor = SystemColors.Window;
			button15.BackColor = SystemColors.Window;

		}
	}
}
